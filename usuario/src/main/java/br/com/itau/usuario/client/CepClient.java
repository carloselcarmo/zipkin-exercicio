package br.com.itau.usuario.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CEP", configuration = CepClientConfiguration.class)
public interface CepClient
{
    @GetMapping("/cep/{cep}")
    @NewSpan(name = "API Busca CEP")
    Cep buscarCep(@PathVariable String cep);
}