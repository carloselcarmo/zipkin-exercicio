package br.com.itau.usuario.DTO;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UsuarioEntrada
{
    @NotNull(message =  "Nome não pode ser nulo")
    @NotBlank(message =  "Nome não pode ser branco")
    private String nome;

    @NotNull(message =  "CEP não pode ser nulo")
    @NotBlank(message =  "CEP não pode ser branco")
    private String cep;

    @NotNull(message =  "Número não pode ser nulo")
    private Integer numero;

    private String complemento;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public UsuarioEntrada() {
    }
}
