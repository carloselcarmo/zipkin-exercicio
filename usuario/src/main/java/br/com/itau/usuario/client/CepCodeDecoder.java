package br.com.itau.usuario.client;

import br.com.itau.usuario.exception.CepNaoEncontradoException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CepCodeDecoder implements ErrorDecoder {
    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response)
    {
        if(response.status() == 404)
        {
            return new CepNaoEncontradoException();
        }
        return errorDecoder.decode(s, response);
    }
}