package br.com.itau.usuario.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.BAD_GATEWAY, reason = "A API de CEP não está disponível")
public class APICepNaoDisponivelException extends RuntimeException  {
}