package br.com.itau.usuario.repository;

import br.com.itau.usuario.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<Usuario,Integer >
{
}