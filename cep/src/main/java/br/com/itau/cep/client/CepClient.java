package br.com.itau.cep.client;

import br.com.itau.cep.model.Cep;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "ViaCEP", url = "https://viacep.com.br/")
public interface CepClient {

    @GetMapping("/ws/{cep}/json/")
    @NewSpan(name = "Busca Via CEP")
    Cep getCep(@SpanTag("cep") @PathVariable String cep);
}
