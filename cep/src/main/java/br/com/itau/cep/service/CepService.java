package br.com.itau.cep.service;


import br.com.itau.cep.client.CepClient;
import br.com.itau.cep.model.Cep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.stereotype.Service;

@Service
public class CepService
{
    @Autowired
    private CepClient cepClient;

    @NewSpan(name = "Service do CEP")
    public Cep getByCep(@SpanTag("cep") String cep) {
        return cepClient.getCep(cep);
    }

}
